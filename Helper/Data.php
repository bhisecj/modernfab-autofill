<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Humcommerce\OrderSample\Helper;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Helper\View as CustomerViewHelper;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

/**
 * Contact base helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Customer\Helper\View
     */
    protected $_customerViewHelper;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var array
     */
    private $postData = null;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param CustomerViewHelper $customerViewHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        CustomerViewHelper $customerViewHelper
    ) {
        $this->_customerSession = $customerSession;
        $this->_customerViewHelper = $customerViewHelper;
        parent::__construct($context);
    }


    /**
     * Get user name
     *
     * @return string
     */
    public function getUserName()
    {
         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
         $customerSession = $objectManager->get('Magento\Customer\Model\Session');
         $cust_name='';
         if($customerSession->isLoggedIn()) { 
                // customer login action
                  $cust_name=$customerSession->getCustomer()->getName();  // get  Full Name

             }
        echo $cust_name;
        return trim($cust_name);
    }

    /**
     * Get user email
     *
     * @return string
     */
    public function getUserEmail()
    {
         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
         $customerSession = $objectManager->get('Magento\Customer\Model\Session');
         $cust_email='';
         if($customerSession->isLoggedIn()) { 
                // customer login action
            $cust_email=$customerSession->getCustomer()->getEmail(); // get Email 
             }
         echo $cust_email;
         return trim($cust_email);
    }
    
    public function getProductName()
    {
         $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
         if(isset($_POST['product_id'])){

	 $product_id=$_POST['product_id'];
	 $currentproduct = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
         $product_name=$currentproduct->getName(); 
	}
	else{
		$product_name='';
	}
         echo $product_name;
	 return trim($product_name);
    }

    /**
     * Get value from POST by key
     *
     * @param string $key
     * @return string
     */
    public function getPostValue($key)
    {
        if (null === $this->postData) {
            $this->postData = (array) $this->getDataPersistor()->get('contact_us');
            $this->getDataPersistor()->clear('contact_us');
        }

        if (isset($this->postData[$key])) {
            return (string) $this->postData[$key];
        }

        return '';
    }
    
    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }
}

<?php
/**
 * Description of OrderSample
 *
 * @author hbwsl
 */

namespace Humcommerce\OrderSample\Controller\Index;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action
{      
    protected $resultPageFactory;
    protected $_registry;

    public function __construct(\Magento\Framework\App\Action\Context $context, PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
     public function execute()
   {
    /*     $productid=$_POST['product_id'];
         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
         $customerSession = $objectManager->get('Magento\Customer\Model\Session');
         
        $currentproduct = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
        $product_name=$currentproduct->getName(); 
        if($customerSession->isLoggedIn()) {
       
// customer login action
             $cust_name=$customerSession->getCustomer()->getName();  // get  Full Name
             $cust_email=$customerSession->getCustomer()->getEmail(); // get Email   
     //        echo $cust_email;

             }
             
        $resultPage = $this->resultPageFactory->create();
     //   $block = $this->_view->getLayout()->createBlock('OrderSample\Block\Adminhtml\CustomBlock', 'block name', ['data' => ['MyData' => 'value']]);
//        $this->$registry->register("product_name", $product_name);
        
        $block = $this->_view->getLayout()->createBlock('OrderSample\Block\Sample\Ordersample', 'block name', ['data' => ['MyData' => 'value']]);
        return $resultPage;
     */
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
